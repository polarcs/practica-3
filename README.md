# **Para ver el resultado ir al siguiente enlace.** [AQUÍ](https://polarcs.gitlab.io/practica-3/)

# Práctica 3: Menús, Estructura CSS, Texto, Fuentes, Imágenes y Multimedia
El enunciado del ejercicio y los materiales para su realización se encuentran disponibles para descarga  "Ejercicio P2P obligatorio. Fichero del enunciado."


### Instrucciones para la Entrega.
La entrega debe incluir el URL de la página Web (o app) publicada en Internet en la descripción de la entrega con un texto como el siguiente:

> "Mi entrega está accesible en: http://nombre_del_grupo.neocities.org/index.html para su evaluación".  


### Instrucciones para la evaluación por pares.
 
Para evaluar la entrega, el evaluador debe comprobar que la entrega es correcta descargando el fichero entregado.
Dado que es un curso para principiantes, ante la duda les pedimos que sean benevolentes con sus compañeros, porque muchos participantes estan empezando con HTML5 y los primeros pasos siempre son difíciles. 

El **objetivo** de este curso es sacar el máximo provecho al trabajo que están dedicando, por lo que les recomendamos que utilicen la evaluación para ayudar a sus compañeros enviando comentarios sobre la corrección del código, su claridad, legibilidad, estructuración y documentación. 
